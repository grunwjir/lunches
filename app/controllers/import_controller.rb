class ImportController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def index

  end

  def update
    render json: 'a'
  end

  def test
    importer = Importer.new()
    info = importer.importTest(params['_json'])
    render json: info
  end

  def deleteDB
    Import.delete_all
    PairedDeposit.delete_all
    TakenMeal.delete_all
    Meal.delete_all
    Deposit.delete_all

    User.all.each do |user|
      user.account = 0
      user.save
    end

    render nothing: true, status: 200, content_type: 'text/html'
  end

end