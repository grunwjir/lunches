class TakenMealsController < ApplicationController
  before_action :set_taken_meal, only: [:show, :edit, :update, :destroy]

  # GET /taken_meals
  # GET /taken_meals.json
  def index
    render "index", :locals => {:taken_meals => TakenMeal.all, :paired_deposits => PairedDeposit.all}
  end

  # GET /taken_meals/1
  # GET /taken_meals/1.json
  def show
  end

  # GET /taken_meals/new
  def new
    @taken_meal = TakenMeal.new
  end

  # GET /taken_meals/1/edit
  def edit
  end

  # POST /taken_meals
  # POST /taken_meals.json
  def create
    @taken_meal = TakenMeal.new(taken_meal_params)

    respond_to do |format|
      if @taken_meal.save
        format.html { redirect_to @taken_meal, notice: 'Taken meal was successfully created.' }
        format.json { render :show, status: :created, location: @taken_meal }
      else
        format.html { render :new }
        format.json { render json: @taken_meal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /taken_meals/1
  # PATCH/PUT /taken_meals/1.json
  def update
    respond_to do |format|
      if @taken_meal.update(taken_meal_params)
        format.html { redirect_to @taken_meal, notice: 'Taken meal was successfully updated.' }
        format.json { render :show, status: :ok, location: @taken_meal }
      else
        format.html { render :edit }
        format.json { render json: @taken_meal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /taken_meals/1
  # DELETE /taken_meals/1.json
  def destroy
    @taken_meal.destroy
    respond_to do |format|
      format.html { redirect_to taken_meals_url, notice: 'Taken meal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_taken_meal
      @taken_meal = TakenMeal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def taken_meal_params
      params.require(:taken_meal).permit(:date, :user_id, :meal_id)
    end
end
