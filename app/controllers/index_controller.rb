class IndexController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception



  def meals
    meals =  Meal.select('meals.date, meals.name, meals.description, meals.pieces, meals.price, meals.discount, users.username, users.surname, users.account, users.firstname, users.id as userId, meals.id as mealId, taken_meals.id as id').
        joins('LEFT JOIN taken_meals ON taken_meals.meal_id = meals.id').
        joins('LEFT JOIN users ON taken_meals.user_id = users.id')
    render json: meals
  end

  def deposits
    deposits =  Deposit.select('users.username, users.surname, users.account, users.firstname, users.id as userId, paired_deposits.id as id, deposits.amount, deposits.date, deposits.id as depositId').
        joins('LEFT JOIN paired_deposits ON paired_deposits.deposit_id = deposits.id').
        joins('LEFT JOIN users ON paired_deposits.user_id = users.id')
    render json: deposits
  end

  def assign_meal
    ActiveRecord::Base.transaction do
      mealId = params['mealId'].to_i
      userId = params['userId'].to_i


      user = User.find(userId)
      meal = Meal.find(mealId)

      assign = TakenMeal.new
      assign.date = Date.new
      assign.user = user
      assign.meal = meal

      price = meal.price
      user.account -= price

      assign.save
      user.save
    end

    render nothing: true, status: 200, content_type: 'text/html'
  end


  def unassign_meal
    ActiveRecord::Base.transaction do
      assignId = params['assignId'].to_i

      assign = TakenMeal.find(assignId)
      user = assign.user

      user.account += assign.meal.price


      user.save
      assign.destroy
    end

    render nothing: true, status: 200, content_type: 'text/html'
  end


  def assign_deposit
    ActiveRecord::Base.transaction do
      depositId = params['depositId'].to_i
      userId = params['userId'].to_i


      user = User.find(userId)
      deposit = Deposit.find(depositId)

      assign = PairedDeposit.new
      assign.date = Date.new
      assign.user = user
      assign.deposit = deposit

      amount = deposit.amount
      user.account += amount

      assign.save
      user.save
    end

    render nothing: true, status: 200, content_type: 'text/html'
  end

  def unassign_deposit
    ActiveRecord::Base.transaction do
      assignId = params['assignId'].to_i

      assign = PairedDeposit.find(assignId)
      user = assign.user

      user.account -= assign.deposit.amount


      user.save
      assign.destroy
    end

    render nothing: true, status: 200, content_type: 'text/html'
  end

end