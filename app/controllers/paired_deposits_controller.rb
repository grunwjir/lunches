class PairedDepositsController < ApplicationController
  before_action :set_paired_deposit, only: [:show, :edit, :update, :destroy]

  # GET /paired_deposits
  # GET /paired_deposits.json
  def index
    @paired_deposits = PairedDeposit.all
  end

  # GET /paired_deposits/1
  # GET /paired_deposits/1.json
  def show
  end

  # GET /paired_deposits/new
  def new
    @paired_deposit = PairedDeposit.new
  end

  # GET /paired_deposits/1/edit
  def edit
  end

  # POST /paired_deposits
  # POST /paired_deposits.json
  def create
    @paired_deposit = PairedDeposit.new(paired_deposit_params)

    respond_to do |format|
      if @paired_deposit.save
        format.html { redirect_to @paired_deposit, notice: 'Paired deposit was successfully created.' }
        format.json { render :show, status: :created, location: @paired_deposit }
      else
        format.html { render :new }
        format.json { render json: @paired_deposit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /paired_deposits/1
  # PATCH/PUT /paired_deposits/1.json
  def update
    respond_to do |format|
      if @paired_deposit.update(paired_deposit_params)
        format.html { redirect_to @paired_deposit, notice: 'Paired deposit was successfully updated.' }
        format.json { render :show, status: :ok, location: @paired_deposit }
      else
        format.html { render :edit }
        format.json { render json: @paired_deposit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paired_deposits/1
  # DELETE /paired_deposits/1.json
  def destroy
    @paired_deposit.destroy
    respond_to do |format|
      format.html { redirect_to paired_deposits_url, notice: 'Paired deposit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_paired_deposit
      @paired_deposit = PairedDeposit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def paired_deposit_params
      params.require(:paired_deposit).permit(:date, :user_id, :deposit_id)
    end
end
