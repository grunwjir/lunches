require 'nokogiri'
require 'ostruct'

class Parser

  ROW_ITEMS = 6

  def initialize(html_doc)
    @html_doc = html_doc
  end

  def parse
    data_container = @html_doc.css("div.container")[2]
    rows = data_container.css("tbody tr")
    parsed_data = {}
    items = Array.new
    rows.each do |row|
      cols = row.css("td")
      # skip last row
      next if cols.size == 2
      # date or title row
      if cols.size == 1
        date = row.css("td[style*=\"color: #9e9e9e;\"]")
        # skip title row
        next if date.size == 0
        parsed_data[DateTime.strptime(date.text, '%d. %m. %Y %H:%M:%S')] = items
        items = Array.new
      elsif
      items << self.parse_row_item(cols)
      end
    end
    parsed_data
  end

  def parse_row_item(cols)
    raise 'Wrong row columns size' if cols.size != ROW_ITEMS
    item = OpenStruct.new
    item.place = cols[0].text
    item.name = cols[2].text
    item.pieces = cols[3].text.tr(',', '.').to_f
    item.description = cols[4].text
    item.price = cols[5].text.tr(',', '.').to_f
    item
  end

end