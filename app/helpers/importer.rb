require 'parser'
require 'date'
require 'ostruct'

class Importer

  DISCOUNT_VALUE = 22.25

  def importTest(date)

    last_import_log = Import.last
    if last_import_log
      last_update = last_import_log.date
    end

    total_meals_size = 0
    total_deposits_size = 0

    parser = Parser.new(File.open("file/lunches_#{date}.html") { |f| Nokogiri::HTML(f) })

    data = parser.parse

    data.each do |date, values|
      # skip already imported
      next if last_update && date <= last_update
      discount = 0
      meals = []
      deposits = []
      values.each do |value|
        if value.description == 'Vklad na konto'
          deposits << create_deposit(value, date)
        elsif value.description == 'Příspěvek na jídlo'
          discount += value.price
        else
           meals << create_meal(value, date) if value.name.size > 1
        end
      end
      add_discounts(meals, discount)
      save_meals(meals)
      save_deposits(deposits)
      # save import log
      save_import_log(date, meals.size, deposits.size)
      total_meals_size += meals.size
      total_deposits_size += deposits.size
    end
    {:meals => total_meals_size, :deposits => total_deposits_size}
  end

  private
  def create_deposit(item, date)
    deposit = Deposit.new
    deposit.amount = item.price
    deposit.date = date
    deposit.place = item.place
    deposit
  end

  def create_meal(item, date)
    meal = Meal.new
    meal.name = item.name
    meal.description = item.description
    meal.pieces = item.pieces
    meal.price = item.price
    meal.place = item.place
    meal.date = date
    meal
  end

  def add_discounts(meals, discount)
    discount_size = (discount / DISCOUNT_VALUE).to_i

    return if discount_size == 0

    sorted_meals = meals.sort { |a, b| a.price <=> b.price }.reverse!

    min_price = sorted_meals[discount_size - 1].price

    meals.each do |meal|
      if meal.price >= min_price
        meal.price -= DISCOUNT_VALUE
        meal.discount = true
      end
    end
    meals
  end

  def save_meals(meals)
    meals.each do |meal|
      meal.save
    end
  end

  def save_deposits(deposits)
    deposits.each do |deposit|
      deposit.save
    end
  end

  def save_import_log(date, meals_size, deposits_size)
    log = Import.new
    log.date = date
    log.deposits = deposits_size
    log.items = meals_size
    log.save
  end
end