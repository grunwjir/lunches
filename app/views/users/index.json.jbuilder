json.array!(@users) do |user|
  json.extract! user, :id, :username, :firstname, :surname, :password, :email, :account
  json.url user_url(user, format: :json)
end
