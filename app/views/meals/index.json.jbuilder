json.array!(@meals) do |meal|
  json.extract! meal, :id, :place, :name, :pieces, :description, :price, :date, :discount
  json.url meal_url(meal, format: :json)
end
