json.array!(@paired_deposits) do |paired_deposit|
  json.extract! paired_deposit, :id, :date, :user_id, :deposit_id
  json.url paired_deposit_url(paired_deposit, format: :json)
end
