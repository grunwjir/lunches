json.array!(@taken_meals) do |taken_meal|
  json.extract! taken_meal, :id, :date, :user_id, :meal_id
  json.url taken_meal_url(taken_meal, format: :json)
end
