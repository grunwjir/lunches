json.array!(@deposits) do |deposit|
  json.extract! deposit, :id, :place, :date, :amount
  json.url deposit_url(deposit, format: :json)
end
