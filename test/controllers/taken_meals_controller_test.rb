require 'test_helper'

class TakenMealsControllerTest < ActionController::TestCase
  setup do
    @taken_meal = taken_meals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:taken_meals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create taken_meal" do
    assert_difference('TakenMeal.count') do
      post :create, taken_meal: { author: @taken_meal.author, date: @taken_meal.date, meal: @taken_meal.meal }
    end

    assert_redirected_to taken_meal_path(assigns(:taken_meal))
  end

  test "should show taken_meal" do
    get :show, id: @taken_meal
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @taken_meal
    assert_response :success
  end

  test "should update taken_meal" do
    patch :update, id: @taken_meal, taken_meal: { author: @taken_meal.author, date: @taken_meal.date, meal: @taken_meal.meal }
    assert_redirected_to taken_meal_path(assigns(:taken_meal))
  end

  test "should destroy taken_meal" do
    assert_difference('TakenMeal.count', -1) do
      delete :destroy, id: @taken_meal
    end

    assert_redirected_to taken_meals_path
  end
end
