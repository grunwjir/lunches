require 'test_helper'

class PairedDepositsControllerTest < ActionController::TestCase
  setup do
    @paired_deposit = paired_deposits(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:paired_deposits)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create paired_deposit" do
    assert_difference('PairedDeposit.count') do
      post :create, paired_deposit: { date: @paired_deposit.date, deposit_id: @paired_deposit.deposit_id, user_id: @paired_deposit.user_id }
    end

    assert_redirected_to paired_deposit_path(assigns(:paired_deposit))
  end

  test "should show paired_deposit" do
    get :show, id: @paired_deposit
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @paired_deposit
    assert_response :success
  end

  test "should update paired_deposit" do
    patch :update, id: @paired_deposit, paired_deposit: { date: @paired_deposit.date, deposit_id: @paired_deposit.deposit_id, user_id: @paired_deposit.user_id }
    assert_redirected_to paired_deposit_path(assigns(:paired_deposit))
  end

  test "should destroy paired_deposit" do
    assert_difference('PairedDeposit.count', -1) do
      delete :destroy, id: @paired_deposit
    end

    assert_redirected_to paired_deposits_path
  end
end
