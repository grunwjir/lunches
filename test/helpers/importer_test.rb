require 'test_helper'


class ImporterTest < ActiveSupport::TestCase
  setup do

  end

  test "import meals from 12.2.2016" do
    importer = Importer.new
    # import meals entries from date 12.2.2016
    data = importer.importTest("12_2_2016")
    assert_equal(44, data[:meals], "imported meals")
    assert_equal(6, data[:deposits], "imported deposits")

    meals = Meal.all
    deposits = Deposit.all
    assert_equal(44, meals.size, "saved meals")
    assert_equal(6, deposits.size, "saved deposits")

    discount_meals = 0
    meals.each do |meal|
      discount_meals += 1 if meal.discount
    end
    assert_equal(21, discount_meals, "discount meals")

    total_deposits =  deposits.inject(0) do |sum, deposit|
      sum += deposit.amount
    end

    assert_equal(1400, total_deposits, "total deposits")


  end

  test "import again meals form 12.2.2016" do
    importer = Importer.new
    # import meals entries from date 12.2.2016
    data = importer.importTest("12_2_2016")
    assert_equal(44, data[:meals], "imported meals")
    assert_equal(6, data[:deposits], "imported deposits")
    data = importer.importTest("12_2_2016")
    assert_equal(0, data[:meals], "imported meals")
    assert_equal(0, data[:deposits], "imported deposits")

    meals = Meal.all
    deposits = Deposit.all
    assert_equal(44, meals.size, "saved meals")
    assert_equal(6, deposits.size, "saved deposits")
  end

  test "import meals from 12.2.2016 and 15.2.2016" do
    importer = Importer.new
    # import meals entries from date 12.2.2016
    data = importer.importTest("12_2_2016")
    assert_equal(44, data[:meals], "imported meals 12.2.2016")
    assert_equal(6, data[:deposits], "imported deposits 12.2.2016")
    data = importer.importTest("15_2_2016")
    assert_equal(13, data[:meals], "imported meals 15.2.2016")
    assert_equal(1, data[:deposits], "imported deposits 15.2.2016")

    meals = Meal.all
    deposits = Deposit.all
    assert_equal(57, meals.size, "saved meals")
    assert_equal(7, deposits.size, "saved deposits")
  end
end
