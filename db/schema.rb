# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160217150726) do

  create_table "deposits", force: :cascade do |t|
    t.string   "place"
    t.datetime "date"
    t.float    "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "imports", force: :cascade do |t|
    t.datetime "date"
    t.integer  "items"
    t.integer  "deposits"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "meals", force: :cascade do |t|
    t.string   "place"
    t.string   "name"
    t.float    "pieces"
    t.string   "description"
    t.float    "price"
    t.datetime "date"
    t.boolean  "discount"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "paired_deposits", force: :cascade do |t|
    t.datetime "date"
    t.integer  "user_id"
    t.integer  "deposit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "paired_deposits", ["deposit_id"], name: "index_paired_deposits_on_deposit_id"
  add_index "paired_deposits", ["user_id"], name: "index_paired_deposits_on_user_id"

  create_table "taken_meals", force: :cascade do |t|
    t.datetime "date"
    t.integer  "user_id"
    t.integer  "meal_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "taken_meals", ["meal_id"], name: "index_taken_meals_on_meal_id"
  add_index "taken_meals", ["user_id"], name: "index_taken_meals_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "username"
    t.string   "firstname"
    t.string   "surname"
    t.string   "password"
    t.string   "email"
    t.float    "account"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
