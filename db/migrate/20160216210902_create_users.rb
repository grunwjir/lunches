class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :firstname
      t.string :surname
      t.string :password
      t.string :email
      t.float :account

      t.timestamps null: false
    end
  end
end
