class CreateTakenMeals < ActiveRecord::Migration
  def change
    create_table :taken_meals do |t|
      t.datetime :date
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :meal, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
