class CreateDeposits < ActiveRecord::Migration
  def change
    create_table :deposits do |t|
      t.string :place
      t.datetime :date
      t.float :amount

      t.timestamps null: false
    end
  end
end
