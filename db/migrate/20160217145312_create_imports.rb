class CreateImports < ActiveRecord::Migration
  def change
    create_table :imports do |t|
      t.datetime :date
      t.integer :items
      t.integer :deposits

      t.timestamps null: false
    end
  end
end
