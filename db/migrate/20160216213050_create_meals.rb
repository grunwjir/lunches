class CreateMeals < ActiveRecord::Migration
  def change
    create_table :meals do |t|
      t.string :place
      t.string :name
      t.float :pieces
      t.string :description
      t.float :price
      t.datetime :date
      t.boolean :discount

      t.timestamps null: false
    end
  end
end
