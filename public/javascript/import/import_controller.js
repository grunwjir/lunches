angular.module('lunchesApp').controller('ImportController', ImportController);
ImportController.$inject = ['$http', '$uibModal', '$q'];
function ImportController($http, $uibModal, $q){
    var vm  = this;

    vm.importLunches = importLunches;
    vm.deleteDBData = deleteDBData
    vm.closeAlert = closeAlert;
    vm.alerts = []

    function importLunches(date){
        // block UI
        $http.post('/import/test', JSON.stringify(date))
            .success(function(response) {
                vm.alerts = [];
                var messeage  = 'Import dokončen. Náhráno: ' + response.meals + ' jídel a ' + response.deposits + ' vkladů.';
                vm.alerts.push({ type: 'success', msg: messeage})
            });
    }

    function deleteDBData(){
        $http.post('/import/deleteDB')
            .success(function(response) {
                vm.alerts = [];
                var messeage  = 'Data byla smazána.';
                vm.alerts.push({ type: 'danger', msg: messeage})
            });
    }

    function closeAlert(index) {
        vm.alerts.splice(index, 1);
    };


}