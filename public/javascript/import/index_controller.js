angular.module('lunchesApp').controller('IndexController', IndexController);
IndexController.$inject = ['$http', '$uibModal', '$q'];
function IndexController($http, $uibModal, $q){
    var vm  = this;
    vm.alerts = [];
    vm.assignMeal = assignMeal;
    vm.unassignMeal = unassignMeal;
    vm.assignDeposit = assignDeposit;
    vm.unassignDeposit = unassignDeposit;

    loadData();



    function assignMeal(mealId, userId){
        var data = {"mealId": mealId, "userId": userId};
        $http.post('/index/assignMeal', JSON.stringify(data))
            .success(function(response) {
                loadData();
            });

    }

    function unassignMeal(id){
        var data = {"assignId": id};
        $http.post('/index/unassignMeal', JSON.stringify(data))
            .success(function(response) {
                loadData();
            });

    }

    function assignDeposit(depositId, userId){
        var data = {"depositId": depositId, "userId": userId};
        $http.post('/index/assignDeposit', JSON.stringify(data))
            .success(function(response) {
                loadData();
            });

    }

    function unassignDeposit(id){
        var data = {"assignId": id};
        $http.post('/index/unassignDeposit', JSON.stringify(data))
            .success(function(response) {
                loadData();
            });

    }

    function loadData(){
        var ajaxRequests = [];
        ajaxRequests.push(loadLunches());
        ajaxRequests.push(loadDeposits());
        ajaxRequests.push(loadUsers());

        $q.all(ajaxRequests).then(function(){
            vm.mealUser = {};
            vm.depositUser = {};
            vm.meals.forEach(function(meal){
                vm.mealUser[meal.mealId] = meal.userId;
            });
            vm.deposits.forEach(function(deposit){
                vm.depositUser[deposit.depositId] = deposit.userId;
            });

        });
    }

    function loadLunches(){
        return $http.post('/index/meals')
            .success(function(response) {
                vm.meals = response;
            });
    }

    function loadDeposits(){
        return $http.post('/index/deposits')
            .success(function(response) {
                vm.deposits = response;
            });
    }

    function loadUsers(){
        return $http.get('/users.json')
            .success(function(response) {
                vm.users = response;
            });
    }

    function closeAlert(index) {
        vm.alerts.splice(index, 1);
    };


}