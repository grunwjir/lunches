var app = angular.module("lunchesApp", ['ui.bootstrap', 'treasure-overlay-spinner']);
app.config([
    "$httpProvider", function($httpProvider) {
        $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
    }
])